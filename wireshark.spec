%undefine __cmake_in_source_build
%global plugins_version 3.6
%define _lto_cflags %{nil}

Summary:	Network traffic analyzer
Name:		wireshark
Version:	3.6.14
Release:	6
Epoch:		1
License:	GPL+
Url:		http://www.wireshark.org/

Source0:	https://wireshark.org/download/src/%{name}-%{version}.tar.xz
Source1:        https://www.wireshark.org/download/src/all-versions/SIGNATURES-%{version}.txt
Source2:	90-wireshark-usbmon.rules
Source3:	wireshark.sysusers

Patch2:		wireshark-0002-Customize-permission-denied-error.patch
Patch3:		wireshark-0003-fix-string-overrun-in-plugins-profinet.patch
Patch4:		wireshark-0004-Restore-Fedora-specific-groups.patch
Patch5:		wireshark-0005-Fix-paths-in-a-wireshark.desktop-file.patch
Patch6:		wireshark-0006-Move-tmp-to-var-tmp.patch
Patch7:		wireshark-0007-cmakelists.patch
Patch8:		CVE-2023-3648.patch
Patch9:		CVE-2023-3649.patch
Patch10:	CVE-2023-2906.patch
Patch11:	CVE-2023-4513-1.patch
Patch12:	CVE-2023-4513-2.patch
Patch13:	CVE-2023-4511.patch
Patch14:	CVE-2023-5371.patch
# https://gitlab.com/wireshark/wireshark/-/commit/197e96f05303af0340b7e626f2b15c2edbf350b0
Patch15:        CVE-2023-6175.patch
Patch16:  Fix-libvirt-build-fail.patch

Requires:	xdg-utils
Requires:	hicolor-icon-theme
Requires(pre):	shadow-utils
Requires(post): systemd-udev
Provides:       %{name}-cli = %{epoch}:%{version}-%{release}
Obsoletes:      %{name}-cli < %{epoch}:%{version}-%{release} wireshark-qt wireshark-gtk


BuildRequires:	bzip2-devel
BuildRequires:	c-ares-devel
BuildRequires:	elfutils-devel
BuildRequires:	gcc-c++
BuildRequires:	glib2-devel
BuildRequires:	gnutls-devel
BuildRequires:	krb5-devel
BuildRequires:	libcap-devel
BuildRequires:	libgcrypt-devel
BuildRequires:	libnl3-devel
BuildRequires:	libpcap-devel >= 0.9
BuildRequires:	libselinux-devel
BuildRequires:	libsmi-devel
BuildRequires:	openssl-devel
BuildRequires:	desktop-file-utils
BuildRequires:	xdg-utils
BuildRequires:	bison
BuildRequires:	flex
BuildRequires:	pcre-devel
BuildRequires:  perl
BuildRequires:	perl(Pod::Html)
BuildRequires:	perl(Pod::Man)
BuildRequires:	perl(open)
Buildrequires:	libssh-devel
BuildRequires:	qt5-linguist
BuildRequires:	qt5-qtbase-devel
BuildRequires:	qt5-qtmultimedia-devel
BuildRequires:	qt5-qtsvg-devel
BuildRequires:	zlib-devel
BuildRequires:	asciidoctor
Buildrequires:  git-core
Buildrequires:  cmake
BuildRequires:  systemd-devel
BuildRequires:  systemd
BuildRequires:  libnghttp2-devel
BuildRequires:  pulseaudio

%description
Wireshark allows you to examine protocol data stored in files or as it is
captured from wired or wireless (WiFi or Bluetooth) networks, USB devices,
and many other sources.  It supports dozens of protocol capture file formats
and understands more than a thousand protocols.

It has many powerful features including a rich display filter language
and the ability to reassemble multiple protocol packets in order to, for
example, view a complete TCP stream, save the contents of a file which was
transferred over HTTP or CIFS, or play back an RTP audio stream.

%package devel
Summary:	Development headers and libraries for wireshark
Requires:	%{name} = %{epoch}:%{version}-%{release} glibc-devel glib2-devel

%description devel
The wireshark-devel package contains the header files, developer
documentation, and libraries required for development of wireshark scripts
and plugins.

%package        help
Summary:        This package contains help documents
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description    help
Files for help with wireshark.

%prep
%autosetup -S git

%build
%cmake -G "Unix Makefiles" \
  -DDISABLE_WERROR=ON \
  -DBUILD_wireshark=ON \
  -DENABLE_LUA=OFF \
  -DBUILD_mmdbresolve=OFF \
  -DBUILD_randpktdump=OFF \
  -DBUILD_androiddump=ON \
  -DENABLE_SMI=ON \
  -DENABLE_PLUGINS=ON \
  -DENABLE_NETLINK=ON \
  -DBUILD_dcerpcidl2wrs=OFF \
  -DBUILD_sdjournal=ON \
  %{nil}

%make_build

%install
%make_install

desktop-file-validate %{buildroot}%{_datadir}/applications/org.wireshark.Wireshark.desktop
install -d -m 0755  %{buildroot}%{_includedir}/wireshark
IDIR="%{buildroot}%{_includedir}/wireshark"
mkdir -p "${IDIR}/epan"
mkdir -p "${IDIR}/epan/crypt"
mkdir -p "${IDIR}/epan/ftypes"
mkdir -p "${IDIR}/epan/dfilter"
mkdir -p "${IDIR}/epan/dissectors"
mkdir -p "${IDIR}/epan/wmem"
mkdir -p "${IDIR}/wiretap"
mkdir -p "${IDIR}/wsutil"
mkdir -p %{buildroot}%{_udevrulesdir}
install -m 644 config.h epan/register.h	"${IDIR}/"
install -m 644 cfile.h file.h		"${IDIR}/"
install -m 644 ws_symbol_export.h	"${IDIR}/"
install -m 644 epan/*.h			"${IDIR}/epan/"
install -m 644 epan/crypt/*.h		"${IDIR}/epan/crypt"
install -m 644 epan/ftypes/*.h		"${IDIR}/epan/ftypes"
install -m 644 epan/dfilter/*.h		"${IDIR}/epan/dfilter"
install -m 644 epan/dissectors/*.h	"${IDIR}/epan/dissectors"
install -m 644 wiretap/*.h		"${IDIR}/wiretap"
install -m 644 wsutil/*.h		"${IDIR}/wsutil"
install -m 644 ws_diag_control.h	"${IDIR}/"
install -m 644 %{SOURCE2}		%{buildroot}%{_udevrulesdir}
install -Dpm 644 %{SOURCE3}		%{buildroot}%{_sysusersdir}/%{name}.conf
touch %{buildroot}%{_bindir}/%{name}
find %{buildroot} -type f -name "*.la" -delete

%pre
getent group wireshark >/dev/null || groupadd -r wireshark
exit 0
#%post
#%{?ldconfig}
#if [ -S /run/udev/control ]; then
#    /usr/bin/udevadm trigger --subsystem-match=usbmon
#fi
#
#%ldconfig_postun

%files
%{_datadir}/applications/org.wireshark.Wireshark.desktop
%{_datadir}/metainfo/*.xml
%{_datadir}/mime/packages/*.xml
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/icons/hicolor/*/mimetypes/*
%{_bindir}/*
%{_mandir}/man1/wireshark.*
%{_sysusersdir}/%{name}.conf
%license COPYING
%attr(0750, root, wireshark) %caps(cap_net_raw,cap_net_admin=ep) %{_bindir}/dumpcap
%{_udevrulesdir}/90-wireshark-usbmon.rules
%{_libdir}/lib*.so.*
%dir %{_libdir}/wireshark
%dir %{_libdir}/wireshark/extcap
%dir %{_libdir}/wireshark/plugins
%{_libdir}/wireshark/extcap/*
%dir %{_libdir}/wireshark/cmake
%{_libdir}/wireshark/cmake/*.cmake
%dir %{_libdir}/wireshark/plugins/%{plugins_version}
%dir %{_libdir}/wireshark/plugins/%{plugins_version}/epan
%dir %{_libdir}/wireshark/plugins/%{plugins_version}/wiretap
%dir %{_libdir}/wireshark/plugins/%{plugins_version}/codecs
%{_libdir}/wireshark/plugins/%{plugins_version}/epan/*.so
%{_libdir}/wireshark/plugins/%{plugins_version}/wiretap/*.so
%{_libdir}/wireshark/plugins/%{plugins_version}/codecs/*.so
%dir %{_datadir}/wireshark
%{_datadir}/wireshark/*

%files devel
%{_includedir}/wireshark
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/%{name}.pc

%files help
%doc AUTHORS INSTALL NEWS README* doc/README.* ChangeLog
%{_mandir}/man?/*

%changelog
* Mon Oct 16 2023 zhengting <zhengting13@huawei.com> -1:3.6.14-6
- Fix libvirt build fail problem

* Tue Nov 21 2023 yaoxin <yao_xin001@hoperun.com> - 1:3.6.14-5
- Fix CVE-2023-6175

* Sat Oct 07 2023 wangkai <13474090681@163.com> - 1:3.6.14-4
- Fix CVE-2023-5371

* Wed Sep 06 2023 wangkai <13474090681@163.com> - 1:3.6.14-3
- Fix CVE-2023-3649,CVE-2023-2906,CVE-2023-4511,CVE-2023-4513

* Thu Jul 27 2023 liningjie <liningjie@xfusion.com> - 1:3.6.14-2
- Fix CVE-2023-3648

* Thu Jun 15 2023 wangkai <13474090681@163.com> - 1:3.6.14-1
- Update to 3.6.14 for fix CVE-2023-0667,CVE-2023-2952

* Tue May 30 2023 yaoxin <yao_xin001@hoperun.com> - 1:3.6.11-4
- Fix CVE-2023-0668,CVE-2023-2855,CVE-2023-2856,CVE-2023-2857,CVE-2023-2858 and CVE-2023-2879

* Mon Apr 24 2023 yaoxin <yao_xin001@hoperun.com> - 1:3.6.11-3
- Fix CVE-2023-1992,CVE-2023-1993 and CVE-2023-1994

* Mon Mar 27 2023 yaoxin <yaoxin30@h-partners.com> - 1:3.6.11-2
- Fix CVE-2023-1161

* Tue Feb 14 2023 liyuxiang<liyuxiang@ncti-gba.cn> - 1:3.6.11-1
- Update to 3.6.11

* Wed Nov 09 2022 liyuxiang<liyuxiang@ncti-gba.cn> - 1:3.6.3-3
- fix CVE-2022-3725

* Tue Sep 27 2022 liyuxiang<liyuxiang@ncti-gba.cn> - 1:3.6.3-2
- fix CVE-2022-3190

* Tue Apr 19 2022 wangkai <wangkai385@huawei.com> - 3.6.3-1
- Update to 3.6.3

* Fri Apr 1 2022 yaoxin <yaoxin30@huawei.com> - 2.6.2-21
- Fix CVE-2021-22191 CVE-2021-22207 CVE-2021-4181 CVE-2021-4185

* Tue Jul 27 2021 wangyue <wangyue92@huawei.com> - 2.6.2-20
- fix CVE-2019-12295

* Tue Jul 27 2021 lingsheng <lingsheng@huawei.com> - 2.6.2-19
- Move headers outside extern C to fix build fail

* Thu Feb 25 2021 wangxiao <wangxiao65@huawei.com> - 2.6.2-18
- Fix CVE-2019-13619 CVE-2019-19553 CVE-2020-9428 CVE-2020-9431

* Mon Feb 08 2021 wangyue <wangyue92@huawei.com> - 2.6.2-17
- fix CVE-2020-26575

* Thu Jan 14 2021 zhanghua <zhanghua40@huawei.com> - 2.6.2-16
- fix CVE-2019-16319

* Wed Dec 16 2020 zhanghua <zhanghua40@huawei.com> - 2.6.2-15
- fix CVE-2020-9430

* Mon Nov 07 2020 wangxiao <wangxiao65@huawei.com> - 2.6.2-14
- fix CVE-2020-28030
  malformed packet on wire could make GQUIC protocol dissector loop

* Mon Nov 02 2020 lingsheng <lingsheng@huawei.com> - 2.6.2-13
- Fix use-of-uninitialized-value in end_string

* Wed Oct 21 2020 wangxiao <wangxiao65@huawei.com> - 2.6.2-12
- Type:cves
- ID: CVE-2020-25862 CVE-2020-25863
- SUG:NA
- DESC: fix CVE-2020-25862 CVE-2020-25863

* Wed Sep 15 2020 wangxiao <wangxiao65@huawei.com> - 2.6.2-11
- Type:cves
- ID: CVE-2018-16056
- SUG:restart
- DESC: fix CVE-2018-16056

* Thu Sep 10 2020 baizhonggui <baizhonggui@huawei.com> - 2.6.2-10
- Modify source0

* Wed Aug 05 2020 yaokai <yaokai13@huawei.com> - 2.6.2-9
- Type:cves
- ID: CVE-2020-15466
- SUG:restart
- DESC: fix CVE-2020-15466

* Tue Jul 21 2020 wangyue <wangyue92@huawei.com> - 2.6.2-8
- Type:cves
- ID: CVE-2020-13164
- SUG:restart
- DESC: fix CVE-2020-13164

* Fri May 15 2020 huanghaitao <huanghaitao8@huawei.com> - 2.6.2-7
- Type:cves
- ID: CVE-2020-11647
- SUG:restart
- DESC: fix CVE-2020-11647

* Sun Feb 2 2020 lingyang <lingyang2@huawei.com> - 2.6.2-6
- Type:cves
- ID: CVE-2019-5719
- SUG:restart
- DESC: fix CVE-2019-5719

* Sun Feb 2 2020 gulining<gulining1@huawei.com> - 2.6.2-5
- Type:cves
- ID: CVE-2019-5716 CVE-2019-5717
- SUG:restart
- DESC: fix CVE-2019-5716 CVE-2019-5717

* Wed Dec 25 2019 gulining <gulining1@huawei.com> - 2.6.2-4
- Type:cves
- ID: CVE-2019-10894 CVE-2019-10896 CVE-2019-10899 CVE-2019-10901 CVE-2019-10903 CVE-2019-10895
- SUG:restart
- DESC: fix CVE-2019-10894 CVE-2019-10896 CVE-2019-10899 CVE-2019-10901 CVE-2019-10903 CVE-2019-10895

* Mon Nov 25 2019 gulining<gulining1@huawei.com> - 2.6.2-3
- revise obsoletes

* Wed Nov 13 2019 gulining<gulining1@huawei.com> - 2.6.2-2
- Pakcage init
